## Run the pipeline

### Input
A list of projects has to be provided in the form of a CSV file with the
following fields:
id	pathWithNamespace	createdAt	lastActivityAt	starsCount	forksCount	stages	isBuildStageInStages	isBuildStageInJobs	jobNames	isBuildInJobsName	isDeployStageInStages	isDeployStageInJobs	jobNames	isDeployInJobsName	pipelines	language	languages

Currently, only the fields `pathWithNamespace` and `language` have to be provided.
The rest of the fields can be left empty.

Download our list of projects [here](#).

### Execution
```
$ mvn clean compile test assembly:single
$ java -cp target/ConfigurationAnalytics-0.0.1-SNAPSHOT-jar-with-dependencies.jar ch.uzh.seal.app.ConfigurationAnalytics /path/to/project/list.csv
```

### Output
The following data is generated in the `target/` folder:
* `CI-anti-patterns.csv`: contains all anti-patterns found in the projects
* `processedProjects.txt`: contains the names of the projects which were processed
* A folder `projects` containing the configuration files of the projects (
.gitlab-ci.yml, pom.xml, requirements.txt)

The first time you run the program, all configuration files of a project are 
downloaded and stored locally in the `target/projects` folder. For subsequent runs, 
those local files are used to extract anti-patterns since fetching remote content
is costly. In addition, anti-patterns are only extracted when the projects has not
been processed yet as determined by whether the project is listed in 
`target/processedProjects.txt`. Thus, to re-generate the list of anti-patterns, 
please delete `target/CI-anti-patterns.csv` and `target/processedProjects.txt`.

Download the `projects` folder [here](#).

## Known Issues
* When downloading many projects (>1000), the program will crash due to a stack
overflow error.
* When downloading files from the remote repository, errors such as 
`[Fatal Error] :82:3: The element type "meta" must be terminated by the matching end-tag "</meta>".`
can occur when there is a redirect, instead of 404 error (e.g. when the repository is empty).
* Review [commit](https://bitbucket.org/sealuzh/cd-linter/commits/eb69bf2ed66f8e4e5aad261f17e9cb8203ed5f6a)